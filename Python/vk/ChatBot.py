import json
import requests
import vk_api



namef = 'citiesRus.txt'
def NameCities_1(city):
    d = NameCities(namef)
    new_d = {value: key for key, value in d.items()}
    if city in new_d:
        return new_d[city]

def NameCities(namef):
    f = open(namef, 'r')
    count = 0
    key = ''
    dictCities = {}
    for i in f:
        if count % 2 == 0:
            i = i.strip()
            key = i.lower()
            count += 1
        else:
            i = i.strip()
            dictCities[key] = i
            count += 1
    return(dictCities)

def Rezult(city):
    if city.lower() in NameCities(namef):
        return NameCities(namef)[city.lower()]

def data_save(OUT_, IN_, years, month, id_):
    token = 'd20970f1ea95552694cd7347c8c11d29'
    OUT_ = Rezult(OUT_)
    IN_ = Rezult(IN_)
    if OUT_ == None or IN_ == None or int(years) < 2019 or int(month) > 12 or int(month) < 1:
        write_msg(id_, f"Вы ввели неправильные данные")
        return
    flag = 0
    response = requests.get(f"http://api.travelpayouts.com/v1/prices/calendar?depart_date={years}-{month}&origin={OUT_}&destination={IN_}&token={token}")
    response = json.loads(response.text)
    for data_1 in response['data']:
        data_= response['data'][data_1]
        if data_['airline'] == 'HZ' or data_['airline'] == 'SU':
            flag = 1
            write_msg(id_, f"Рейс = {data_['flight_number']}\n"
                        f"Цена билета: {data_['price']}руб. \n"
                        f"Откуда: {NameCities_1(data_['origin'])} \n"
                        f"Куда: {NameCities_1(data_['destination'])} \n"
                        f"Время и дата вылета: {data_['departure_at']} \n"
                   f"Авиакомпания: {data_['airline']}")
    if flag == 0:
        write_msg(id_, f"Ближайших рейсов на эту дату нет")
        return
def get_data(from_, to_):
    response = requests.get(f"http://aviahack.varanga-studio.com/schedule?from={from_}&to={to_}")
    response = json.loads(response.text)
    return response
def get_data2(object, id):
    response = requests.get(f"http://aviahack.varanga-studio.com/{object}?normType={id}")
    a = json.loads(response.text)
    return  a['result']

from vk_api.longpoll import VkLongPoll, VkEventType

class Data:
    def __init__(self, dataIN = None, dataOUT = None,dataYears = None, dataMonth = None):
        self.dataIN = dataIN
        self.dataOUT = dataOUT
        self.dataYears = dataYears
        self.dataMonth = dataMonth

def get_random_id():
    from random import getrandbits
    from random import choice
    return getrandbits(31) * choice([-1, 1])

def write_msg(user_id, message = None, k = None):
    random_id = get_random_id()
    vk.method('messages.send', {
        'user_id': user_id,
        'keyboard': k,
        'message': message,
        'random_id': random_id
    })

def Avia_company(name):
    write_msg(name, "Выберите следующий пункт", Avia_company_key)
    for event in longpoll.listen():
        if event.type == VkEventType.MESSAGE_NEW:
            if event.to_me:
                request = event.text
                if request == "Аэрофлот":
                    write_msg(event.user_id, get_data2('norm',1) )
                    return
                if request == "Аврора":
                    write_msg(event.user_id, get_data2('norm',2))
                    return

def Baggage_allowance(name):
    write_msg(name, "Выберите следующий пункт", Baggage_allowance_key)
    for event in longpoll.listen():
        if event.type == VkEventType.MESSAGE_NEW:
            if event.to_me:
                request = event.text
                if request == "Внутренние регионы":
                    write_msg(event.user_id, get_data2('norm',0))
                    return
                if request == "Межрегиональные" or request == "Международные":
                    Avia_company(event.user_id)
                    return

def TimeTable(name):
    flag = 1
    write_msg(name, "Введите откуда вы хотите лететь")
    for event in longpoll.listen():
        if event.type == VkEventType.MESSAGE_NEW:
            if event.to_me:
                request = event.text

                if flag == 1:
                    vvod = Data()
                    vvod.dataOUT = request
                    write_msg(event.user_id, "Введите куда вы хотите лететь")
                    flag = 2
                    continue

                if flag == 2:
                    vvod.dataIN = request
                    write_msg(event.user_id, "Введите дату в формате ГГГГ-ММ")
                    flag = 3
                    continue

                if flag == 3:
                    st = request.split('-')
                    vvod.dataYears = st[0]
                    vvod.dataMonth = st[1]
                    data_save(vvod.dataOUT, vvod.dataIN, vvod.dataYears, vvod.dataMonth, event.user_id)
                    # data_mas = get_data(vvod.dataIN, vvod.dataOUT)
                    # for data_ in data_mas:
                    #     write_msg(event.user_id, f"Рейс = {data_['flight']}\n "
                    #     f"Вид транспортного средства: {data_['typeBc']} \n"
                    #     f"Откуда: {data_['cityFrom']} \n"
                    #     f"Куда: {data_['cityTo']} \n"
                    #     f"Время вылета: {data_['departmentTime']} \n"
                    #     f"Время прибытия: {data_['arrivalTime']} \n"
                    #     f"День недели: {data_['weekDay']} \n"
                    #     f"Период: {data_['period']}\n\n")

                    return

def Animals(name):
    write_msg(name, "Выберите следующий пункт", Animals_key)
    for event in longpoll.listen():
        if event.type == VkEventType.MESSAGE_NEW:
            if event.to_me:
                request = event.text
                if request == "В салоне":
                    write_msg(event.user_id, get_data2('animals',0))
                    return
                if request == "В багаже":
                    write_msg(event.user_id, get_data2('animals',1))
                    return
                if request == "Перевозка на воздушном судне DHC-6":
                    write_msg(event.user_id, get_data2('animals',2))
                    return
                if request == "Собаки-проводники":
                    write_msg(event.user_id, get_data2('animals',3))
                    return
                if request == "Вес больше 50":
                    write_msg(event.user_id, get_data2('animals',4))
                    return
                if request == "Оплата":
                    write_msg(event.user_id, get_data2('animals',5))
                    return
                if request == "Документы":
                    write_msg(event.user_id, get_data2('animals',6))
                    return
                if request == "Доп информация":
                    write_msg(event.user_id, get_data2('animals',7))
                    return

def Rules(name):
    write_msg(name, "Выберите следующий пункт", Rules_key)
    for event in longpoll.listen():
        if event.type == VkEventType.MESSAGE_NEW:
            if event.to_me:
                request = event.text
                if request == "Нормы багажа":
                    Baggage_allowance(event.user_id)
                    return
                if request == "Перевозка домашних животных":
                    Animals(event.user_id)
                    return
                if request == "Перевозка спорт инвентаря":
                    write_msg(event.user_id, 'Лыжное оборудование, хоккейное оборудование, велосипеды, оборудование для гольфа, рыболовное оборудование\n'
                                             'Расценивается как 1 место и входит в норму бесплатного провоза багажа следующее оборудование:\n'
                                             'Лыжное оборудование: чехол с 1 парой лыж и 1 парой палок + 1 место багажа с 1 парой ботинок 1 чехол с 1 парой водных лыж\n'
                                             'Сноубордическое оборудование: 1 чехол с 1 сноубордом + 1 место багажа с 1 парой ботинок\n'
                                             'Хоккейное оборудование: чехол с экипировкой + чехол с 2 клюшками\n'
                                             'Велосипед: Велосипед, подготовленный к транспортировке\n'
                                             'Оборудование для гольфа: оборудование для гольфа, упакованное в один чехол\n'
                                             'рыболовное оборудование (упакованное в 1 контейнер или чехол): 2 удочки комплект снастей \n')
                    return


token = "6ef9e1b68cad52e3612541d117737e3e6b4d6b980a487e53b5890398de25c46376557896d61a7dbdd8f64"
vk = vk_api.VkApi(token=token)
longpoll = VkLongPoll(vk)

def GetButtons(text, color = "default"):
    return  {"action": {
                  "type": "text",
                  "payload": "{\"button\": \"1\"}",
                  "label": text
                    },
        "color": color}
Start_key = {
    "one_time": True,
    "buttons": [
      [
        GetButtons("Расписание")
      ],
      [
        GetButtons("Правила перевозок"),
        GetButtons("Бронирование перевозок")
      ],
      [
        GetButtons("Операции с бронированием"),
        GetButtons("Проверка наличия мест")]
      ]
}
Start_key = json.dumps(Start_key, ensure_ascii = False).encode('utf-8')

Rules_key = {
    "one_time": True,
    "buttons": [
      [
        GetButtons("Нормы багажа")
      ],
      [
        GetButtons("Перевозка домашних животных"),
        GetButtons("Перевозка спорт инвентаря"),
      ]]
  }
Rules_key = json.dumps(Rules_key, ensure_ascii = False).encode('utf-8')

Baggage_allowance_key = {
    "one_time": True,
    "buttons": [
        [
            GetButtons("Внутренние регионы"),
            GetButtons("Межрегиональные")
        ],
        [
            GetButtons("Международные")
        ]]
}
Baggage_allowance_key = json.dumps(Baggage_allowance_key, ensure_ascii = False).encode('utf-8')

Animals_key = {
    "one_time": True,
    "buttons": [
      [
        GetButtons("В салоне"),
        GetButtons("В багаже")
      ],
      [
        GetButtons("Перевозка на воздушном судне DHC-6"),
        GetButtons("Собаки-проводники"),
      ],
      [
        GetButtons("Вес больше 50"),
        GetButtons("Оплата"),
      ],
      [
        GetButtons("Документы"),
        GetButtons("Доп информация"),
      ]]
  }
Animals_key = json.dumps(Animals_key, ensure_ascii = False).encode('utf-8')

Avia_company_key ={
"one_time": True,
"buttons":
    [
        [GetButtons("Аэрофлот"),
        GetButtons("Аврора")]
    ]

}
Avia_company_key = json.dumps(Avia_company_key, ensure_ascii = False).encode('utf-8')



for event in longpoll.listen():
    if event.type == VkEventType.MESSAGE_NEW:
        if event.to_me:
            request = event.text


            if request == "Расписание":
                TimeTable(event.user_id)
                write_msg(event.user_id, "Выберите один из пунктов", Start_key)
                continue

            if request == "Правила перевозок":
                Rules(event.user_id)
                write_msg(event.user_id, "Выберите один из пунктов", Start_key)
                continue

            if request == "Бронирование перевозок":
                write_msg(event.user_id, "Перейдите по ссылке:\n"
                                         "https://www.flyaurora.ru/buy/services/")
                write_msg(event.user_id, "Выберите один из пунктов", Start_key)
                continue

            if request == "Операции с бронированием":
                write_msg(event.user_id, "Перейдите по ссылке:\n"
                                         "https://www.flyaurora.ru/buy/services/")
                write_msg(event.user_id, "Выберите один из пунктов", Start_key)
                continue

            if request == "Проверка наличия мест":
                write_msg(event.user_id, "Перейдите по ссылке:\n"
                                         "https://www.flyaurora.ru/buy/services/")
                write_msg(event.user_id, "Выберите один из пунктов", Start_key)
                continue



            write_msg(event.user_id, "Выберите один из пунктов", Start_key)


