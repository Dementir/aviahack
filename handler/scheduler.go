package handler

import (
	"fmt"
	"game-courses-backend/shared/logger"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"
)

const token = "d20970f1ea95552694cd7347c8c11d29"

type Schedule struct {
	Flight            string `json:"flight"`
	TypeBc            string `json:"typeBc"`
	CityFrom          string `json:"cityFrom"`
	CityTo            string `json:"cityTo"`
	DepartmentHour    string `json:"departmentHour"`
	DepartmentMinutes string `json:"departmentMinutes"`
	ArrivalTime       string `json:"arrivalTime"`
	WeekDay           string `json:"weekDay"`
	Period            string `json:"period"`
}

func GetScheduler(c *gin.Context) {
	from := c.Query("from")
	to := c.Query("to")
	logger.L.Info("From:", from, "to: ", to)

	resp, err := http.Get("http://api.travelpayouts.com/v1/prices/calendar?depart_date=" + strconv.Itoa(time.Now().Day()) + "-" + time.Now().Month().String() + "&origin=ОМСК&destination=OMS&token=" + token)

	if err != nil {
		fmt.Println(err)
		c.Status(http.StatusInternalServerError)
		return
	}
	defer resp.Body.Close()

	by, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		fmt.Println(err)
		c.Status(http.StatusInternalServerError)
		return
	}

	//resp := []Schedule{{
	//	Flight:            "HZ-526",
	//	CityFrom:          "Пластун",
	//	CityTo:            "Владивосток",
	//	TypeBc:            "DHC-6",
	//	DepartmentHour:    "13",
	//	DepartmentMinutes: "35",
	//	ArrivalTime:       "15:10",
	//	WeekDay:           "Вт Чт Сб",
	//	Period:            "02.05.19 - 28.09.19",
	//}}

	c.JSON(200, by)
}
