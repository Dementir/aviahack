package handler

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

type norm int

const (
	in animalce = iota
	airflot
	avrora
)

func GetNorm(c *gin.Context) {
	normType := c.Query("normType")
	var result string
	switch getTypeNorm(normType) {
	case in:
		result = "Норма и вес бесплатного провоза багажа и ручной клади на рейсе: \n Зарегистрированный багаж: 1 место 10 кг\nРучная кладь: 1 место 5 кг"
	case airflot:
		result = `Норма и вес бесплатного провоза багажа и ручной клади на рейсе:\nЗарегистрированный багаж: 1 место 23 кг\nРучная кладь: 1 место 10 кг`
	case avrora:
		result = `Норма и вес бесплатного провоза багажа и ручной клади на рейсе:\nЗарегистрированный багаж: 1 место 23 кг \n Ручная кладь: 1 место 5 кг`
	}
	c.JSON(http.StatusOK, struct {
		Result string `json:"result"`
	}{result})
}

func getTypeNorm(query string) animalce {
	switch query {
	case "0":
		return in
	case "1":
		return airflot
	case "2":
		return avrora
	}
	return in
}
