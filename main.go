package main

import (
	"aviahack/router"
	"context"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"time"
)

func main() {
	r := gin.Default()

	router.InitRoutes(r)

	port := ":" + strconv.Itoa(9999)
	server := &http.Server{
		Addr:    port,
		Handler: r,
	}

	go func() {

		fmt.Println("API HTTP server started with pid", syscall.Getpid(), "and port ", port)
		if err := server.ListenAndServe(); err != nil {
			fmt.Println("HTTP server server err: ", err)
		}

	}()
	shutdown := make(chan os.Signal)
	signal.Notify(shutdown, os.Interrupt)

	<-shutdown

	fmt.Println("Shutdown server ...")
	ctx, cancle := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancle()
	if err := server.Shutdown(ctx); err != nil {
		fmt.Println("Server shutdown err: ", err)
	}
	fmt.Println("Server exiting")
}
