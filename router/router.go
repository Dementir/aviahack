package router

import (
	"aviahack/handler"
	"github.com/gin-gonic/gin"
)

func InitRoutes(r *gin.Engine) {
	r.GET("/schedule", handler.GetScheduler)
	r.GET("/animals", handler.GetAnnimalse)
	r.GET("/norm", handler.GetNorm)
}
